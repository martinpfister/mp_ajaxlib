plugin.tx_mpajaxlib {
    view {
        templateRootPaths.0 = {$plugin.tx_mpajaxlib.view.templateRootPath}
        partialRootPaths.0 = {$plugin.tx_mpajaxlib.view.partialRootPath}
        layoutRootPaths.0 = {$plugin.tx_mpajaxlib.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_mpajaxlib.persistence.storagePid}
    }
    settings {
        ajaxPageType = {$plugin.tx_mpajaxlib.settings.ajaxPageType}
    }
}
