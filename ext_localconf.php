<?php
defined('TYPO3_MODE') or die ('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Pfister.' . $_EXTKEY,
    'PiExample',
    [
        'Image' => 'loadMultipleImages'
    ],
    // non-cacheable actions
    [
        'Image' => 'loadMultipleImages',
    ]
);

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['typoscript_rendering']['renderClasses']['hello'] = 'Pfister\\AjaxExample\\Renderer\\HelloWorldRenderer';